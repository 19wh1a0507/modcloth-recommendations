from flask import Flask, render_template, request
import pickle
import numpy as np

fitmodel = pickle.load(open('fitmodel_pickle.pkl', 'rb'))
sizemodel = pickle.load(open('sizemodel_pickle.pkl', 'rb'))


app = Flask(__name__)



@app.route('/')
def man():
    return render_template('home.html')

@app.route('/predict', methods=['POST'])
def home():
    data1 = request.form['a']
    data2 = request.form['b']
    data3 = request.form['c']
    data4 = request.form['d']
    data5 = request.form['e']
    data6 = request.form['f']
    data7 = request.form['g']
    arr = np.array([[data1, data2, data3, data4, data5, data6]])
    size = sizemodel.predict(arr)
    arr2 = np.array([[data1, data2, data3, data4, data5, data6, size]])
    pred = fitmodel.predict(arr2)
    return render_template('after.html', a=pred, data=size, category=data7)


if __name__ == "__main__":
    app.run(debug=True)