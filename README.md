# Modcloth Recommendations

Product size recommendation and fit prediction are critical in order to improve customers’ shopping experiences and to reduce product return rates. 

However, modeling customers’ fit feedback is challenging due to its subtle semantics, arising from the subjective evaluation of products and imbalanced label distribution (most of the feedbacks are "Fit"). 

These datasets, which are the only fit related datasets available publically at this time, collected from ModCloth and RentTheRunWay could be used to address these challenges to improve the recommendation process.

## Content
Following type of information is available in the datasets:

ratings and reviews

fit feedback (small/fit/large)

customer/product measurements

category information


## Dataset 

mod_cloth_final_data.json

## Info

Number of customers: 47,958

Number of products: 1,378

Number of transactions: 82,790

## Field Description

item_id: unique product id

waist: waist measurement of customer

size: the standardized size of the product

quality: rating for the product

cup size: cup size measurement of customer

hips: hip measurement of customer

bra size: bra size of customer

category: the category of the product

bust: bust measurement of customer

height: height of the customer

length: feedback on the length of the product

fit: fit feedback

user_id: a unique id for the customer

shoe size: shoe size of the customer

shoe width: shoe width of the customer

review_text: review of customer

review_summary: review summary

## Output
  --><ins>Home page</ins>
    ![](/images/home.png) -->
    <!-- <img src = "/images/home.png", width="120" height="120"> -->
    <!-- [<img src="/images/home.png" width="32" height="32">](/images/home.png) -->
  <ins>Predict page</ins>
    ![](images/predict1.png)
    ![](images/predict2.png)
    ![](images/predict3.png)
